
This repo is for exploring client applications for the APS.


Road map:
=========

1. operational APS ward-round tool
- mock cloud-based tested server
- mock application
2. inclusion of pathology data
3. clinical decision support
4. extension to morning of surgery tool
5. extension to preoperative assement
6. extension to audit
7. extension to APS referral
8. integration with VERDI (so ward staff can see assessment)

Potential applications for decision support:
============================================

* need for consultant review
* need for discharge from APS
* drug-pathology interaction (renal and hepatic function)
* PCA cessation

Application services
====================

This is the break up of the `angular` application

* msh - provide "organisation interface" ie authentication, storage
* aps - service level service patient list
* cds - clinical decision support


Overall engineering goals:
==========================

1. scalable
2. testable
3. immediately useful
4. integrated
5. separation of concerns

Technology
==========

See [overarching project repository](https://bitbucket.org/erichbschulz/mmla) for details

Gulp
====

 Gulp tasks:

* `$ gulp` to build an optimized version of your application in folder dist
* `$ gulp serve` to start BrowserSync server on your source files with live reload
* `$ gulp serve:dist` to start BrowserSync server on your optimized application without live reload
* `$ gulp test` to run your unit tests with Karma
* `$ gulp test:auto` to run your unit tests with Karma in watch mode
* `$ gulp protractor` to launch your e2e tests with Protractor
* `$ gulp protractor:dist` to launch your e2e tests with Protractor on the dist files

More details are available in [docs and recipes](https://github.com/Swiip/generator-gulp-angular/tree/master/docs)

