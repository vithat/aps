(function() {
'use strict';

angular
.module('aps')
.factory('apsService', apsService);

/** @ngInject */
function apsService($http, $log, $q, $timeout, $window, mhs) {

  /**
  * Private variables:
  */
  var _ = $window._;
  var moment = $window.moment;
  var button_style = {selected: "btn-success", unselected: "btn-default"};

  var server_url = function() {
    return mhs.getServerUrl();
  }

  var api_url = function() {
    return server_url() + "/api/v1/apsp";
  }
  var mock_delay = mhs.mock_delay;

  var test_patients = [
    {id: 1, name: 'Frank', diagnosis: 'uncertain', ward: '8b', bed: 15},
    {id: 3, name: 'Erich', diagnosis: 'manic', ward: '8b', bed: 16},
    {id: 5, name: 'Conrad', diagnosis: 'barking', ward: '9b', bed: 15},
    {id: 6, name: 'Peter', diagnosis: 'kindly', ward: '9b', bed: 17},
    {id: 7, name: 'Mark', diagnosis: 'depressed', ward: '8b', bed: 17},
    {id: 8, name: 'Sue', diagnosis: 'hungry', ward: '8b', bed: 18}
    ];


    var FHIR = {
      "category": ["complaint", "symptom", "finding", "diagnosis"],
      "clinicalStatus": ["active", "relapse", "remission", "resolved"],
      "verificationStatus": ["provisional", "differential", "confirmed", "refuted", "entered-in-error", "unknown"],
      "severity": ["not-stated", "mild", "moderate", "severe"]
    };

  var therapies = ['PCA', 'single shot block', 'block catheter', 'spinal morphine', 'epidural', 'ketamine infusion'];

  var conditions = ['diabetes', 'sleep apnoea', 'epilepsy', 'chronic pain', 'COAD', 'asthma', 'pre-eclampsia'];

  var medication = ['panadol', 'antinflamatory', 'targin', 'tramadol', 'PCA'];
  var plans = ['start', 'continue', 'cease', 'decreae', 'inrease'];
  var simple_scale = ["not assessed", 'none', 'mild', 'moderate', 'severe'];
  var zero_to_10 = ["not assessed", "0", "1","2","3","4","5","6","7", "8","9", "10"];

  var condition_schema = {
        type: "array",
        title: "Comordidities",
        items: {
          type: "object",
          properties: {
            condition: {type: "string", title: 'Condition', enum: conditions},
            clinicalStatus: {type: "string", title: 'Status', enum: FHIR.clinicalStatus, default: 'active'},
            verificationStatus: {type: "string", title: 'Probability', enum: FHIR.verificationStatus, default: 'confirmed'},
            severity: {type: "string", title: 'Severity', enum: FHIR.severity, default: 'not-stated'},
            notes: {type: "string", title: 'Notes'}
          }
        }
      };

  var therapies_schema = {
        type: "array",
        title: "Therapies",
        items: {
          type: "object",
          title: '-',
          properties: {
            therapy: {type: "string", title: 'Type', enum: therapies}
          }
        }
      };

  var base_schema =  {
    type: "object",
    required: [],
    properties: {
      timestamp: {type: "date-time", title: "time" },
      review_plan: {type: "string", title: 'APS plan',
        enum: ['discharge', 'review today', 'review tomorrow'],
        default: 'review tomorrow'
      },
      notes: {type: "string", title: "Notes" },
      nausea: {title: "Nausea", type: "string", enum: simple_scale, default: 'not assessed'},
      sedation: {title: "Sedation", type: "string", enum: simple_scale, default: 'not assessed'},
      itch: {title: "Itch", type: "string", enum: simple_scale, default: 'not assessed'},
      motor_block: {title: "Motor block", type: "string", enum: simple_scale, default: 'not assessed'},
      pain_at_rest: {type: "string", title: 'Pain at rest', enum: zero_to_10, default: 'not assessed'},
      pain_on_movement: {type: "string", title: 'Pain on movement',
        enum: zero_to_10, default: 'not assessed'},
      conditions: condition_schema,
      medication: {
        type: "array",
        title: "Medication",
        items: {
          type: "object",
          properties: {
            medication: {type: "string", title: 'Medication', enum: medication},
            plan: {type: "string", title: 'Plan', enum: plans},
            notes: {type: "string", title: 'Notes'}}
        }
      }
    }
  };


  var base_form =  [{
    type: "tabs", tabs: [{
      title: "Review",
      items: [
        { key: "nausea",
          style: button_style, type: "radiobuttons" },
        { key: "sedation",
          style: button_style, type: "radiobuttons" },
        { key: "pain_at_rest",
          style: button_style, type: "radiobuttons" },
        { key: "pain_on_movement",
          style: button_style, type: "radiobuttons" },
        { key: "itch",
          style: button_style, type: "radiobuttons" },
        { key: "motor_block",
          style: button_style, type: "radiobuttons" }
    ]},
    { title: "Conditions",
      items: ["conditions"] } ,
    { title: "Medication plan",
      items: ["medication"] }
  ]},
  { type: "help", helpvalue: "<p>Help will go here</p>" },
  { type: "conditional",
    condition: "modelData.soul",
    items: [{ key: "soulserial", placeholder: "ex. 666" }] },
  { key: "review_plan",
    style: button_style, type: "radiobuttons" },
  { key: "date_time" },
  { key: "notes",
    type: "textarea" },
  { type: 'actions', items: [
    { type: "submit", style: "btn-success", title: "Save" },
    { type: 'button', style: 'btn-danger', title: 'Cancel', onClick: "pvm.cancel()"}]
  }
  ];


  var register_schema =  {
    type: "object",
    required: [],
    properties: {
      conditions: condition_schema,
      therapies: therapies_schema,
      review_plan: {type: "string", title: 'APS plan',
        enum: ['review today', 'review tomorrow']
      },
      notes: {type: "string", title: "Notes" }
    }
  };

  var register_form =  [
  { key: ["conditions"], add: 'Add condition' } ,
  { key: ["therapies"], title: 'Analgesic therapy', add: 'Add therapy' } ,
  { key: "notes", type: "textarea" },
  { key: "review_plan",
    style: button_style, type: "radiobuttons" },
  { type: 'actions', items: [
    { type: "submit", style: "btn-success", title: "Save" },
    { type: 'button', style: 'btn-danger', title: 'Cancel', onClick: "pvm.cancel()"}]
  }
  ];



  /**
    *
    */
var apsCds = {
  is: function(property, patient) {
      switch (property) {
        case 'obstetric':
          if (patient.name == "Frank") {
            return true;
          }
          else {
            return false;
          }
      }
    }
}

  var service = {
    getPatients: getPatients,
    getPatient: getPatient,
    saveReview: saveReview,
    saveReferral: saveReferral,
    reviewSchema: reviewSchema,
    reviewForm: reviewForm,
    registerSchema: registerSchema,
    registerForm: registerForm
  };

  return service;

  /**
    * Generate a data model based on this patient
    */
function reviewSchema(patient) {
  $log.log('patient', patient);
  var schema = _.cloneDeep(base_schema);
  if (apsCds.is('obstetric', patient)) {
    schema.properties.itch = {
title: "Itch", type: "string", enum: simple_scale, default: 'not assessed'
    };
  }
  return schema;
}

function reviewForm(patient) {
  var form = _.cloneDeep(base_form);
  if (apsCds.is('obstetric', patient)) {
    form[0].tabs[0].items.push(
      {key: "itch", style: button_style, type: "radiobuttons" });
  }
  return form;
}

function registerSchema(patient) {
  var schema = _.cloneDeep(register_schema);
  return schema;
}

function registerForm(patient) {
  var form = _.cloneDeep(register_form);
  if (apsCds.is('obstetric', patient)) {
    form[0].tabs[0].items.push(
      {key: "itch", style: button_style, type: "radiobuttons" });
  }
  return form;
}
function testReviews(uid) {
  if (uid > 10) {
    throw new Error('attempt to generate test review on live data');
  }
  else {
    return [
      {
      time: moment().subtract(3, 'day'),
      sedation: "mild", nausea: "none",
      pain_at_rest: "3", pain_on_movement: "3",
      review_plan: "discharge" },
      {medication: [
        {medication: "panadol", "plan": "continue"},
        {medication: "PCA", "plan": "continue"}],
      time: moment().subtract(2, 'day'),
      sedation: "mild", nausea: "none",
      pain_at_rest: "3", pain_on_movement: "3",
      review_plan: "discharge" },
      { medication: [
        {medication: "panadol", "plan": "continue"},
        {medication: "PCA", "plan": "continue"}],
      time: moment().subtract(1, 'day'),
      sedation: "mild", nausea: "none",
      pain_at_rest: "3", pain_on_movement: "3",
      review_plan: "discharge" }
      ]
  }
}

function getPatients() {
  $log.log('starting get patients');
  if (server_url() == 'mock') {
    return getTestPatients();
  }
  var deferred = $q.defer();
  if (mhs.loggedIn()) {
    var params = mhs.authoriseRequest();
    $http.get(api_url() + '/patients', params)
    .then(function(response) {
      deferred.resolve(response.data.data);
    }, function(response) {
      $log.log('boh response', response);
      deferred.reject('server error: ' + response.status);
    });
  }
  else {
    deferred.reject('not logged in');
  }
  return deferred.promise;
}

function getTestPatients() {
  $log.log('starting get patients');
  var deferred = $q.defer();
  $timeout(function() {
    if (mhs.loggedIn()) {
      deferred.resolve(test_patients);
    }
    else {
      deferred.reject('not logged in');
    }
   }, mock_delay);
  return deferred.promise;
}

function getPatient(id) {
  $log.log('starting get for patient #' + id);
  if (server_url() == 'mock') {
    return getTestPatient(id);
  }
  var deferred = $q.defer();
  if (mhs.loggedIn()) {
    var params = mhs.authoriseRequest();
    $http.get(api_url() + '/patient/' + id, params)
    .then(function(response) {
      deferred.resolve(response.data.data);
    }, function(response) {
      $log.log('bad response', response);
      deferred.reject('server error: ' + response.status);
    });
  }
  else {
    deferred.reject('not logged in');
  }
  return deferred.promise;
}

function saveEvent(id, event_type, rec) {
  $log.log('saving');
  var deferred = $q.defer();
  $http.post(api_url() + '/patient/' + id + '/' + event_type, rec)
  .then(function(response) {
    deferred.resolve(response);
  }, function(response) {
    $log.log('boh response', response);
    deferred.reject('that was not good: ' + response.status);
  });
  return deferred.promise;
}


function getTestPatient(id) {
  $log.log('starting get test patient #' + id);
  var deferred = $q.defer();
  $timeout(function() {
    if (mhs.loggedIn()) {
      $log.log('starting get patient', test_patients);
      var pt = _.find(test_patients,{id: 1*id});
      pt.reviews = testReviews(id);
      deferred.resolve(pt);
    }
    else {
      deferred.reject('not logged in');
    }
   }, mock_delay);
  return deferred.promise;
}

function saveReview(id, review) {
  return saveEvent(id, 'assess', review);
}

function saveReferral(id, model) {
  return saveEvent(id, 'refer', model);
}

}
})();
