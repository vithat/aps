(function() {
'use strict';

angular
.module('aps')
.factory('mhs', mhs);

/** @ngInject */
function mhs($log, $http, $q, $state, $timeout, $window) {

  /**
  * Private variables:
  */
  var mock_delay = 0; // nil
  var user = {};
  var log_in_time;
  var log_in_expires;
  var moment = $window.moment;
  // config object loaded from server
  var config = false;

  var service = {
    configPromise: configPromise,
    getUser: getUser,
    getServerUrl: getServerUrl,
    authoriseRequest: authoriseRequest,
    login: login,
    logout: logout,
    loggedIn: loggedIn,
    mock_delay: mock_delay,
    loginTime: function() {return log_in_time},
    loginExpires: function() {return log_in_expires},
    loggedInOrDeny: loggedInOrDeny
  };

  return service;

function getUser() {
  return user;
}

function getServerUrl() {
  if (config) {
    return config.server_url;
  }
  else {
    $log.log('attempting server access before config');
  }
}

// @returns promise
function configPromise() {
  $log.log('getting config..');
  if (!config) {
    $log.log('.. from server');
    return $http.get('config.json')
    .success(function (data) {
      $log.log('data', data);
      config = data;
      return data;
    });
  }
  else {
    $log.log('.. from cache');
    $q.resolve(config);
  }
}

/**
  * Decorate params with authorisation token
  */
function authoriseRequest(params) {
  params = params || {};
  params.headers = { Authorization: user.token + ';' + user.id};
  return params;
}

function login(id, password) {
  var server_url = getServerUrl();
  $log.log('server_url', server_url);
  var url = server_url + "/user/login";
  if (server_url == 'mock') {
    return mocklogin(id, password);
  }
  var params= {user: id, pass: password};
  var deferred = $q.defer();
  $http.post(url, params)
  .then(function(response) {
    user = {
      id: id,
      name: response.data.name,
      token: response.data.token
    };
    $log.log('user', user);
    log_in_time = new Date();
    log_in_expires = moment(log_in_time).add(20, 'minute');
    $log.log('yah response', response.data);
    deferred.resolve('Hello, #' + id + '!');
  }, function(response) {
    $log.log('boh response', response);
    deferred.reject('that was not good: ' + response.status);
  });
  return deferred.promise;
}

function mocklogin(id, password) {
  var deferred = $q.defer();
  $timeout(function() {
    // deferred.notify('About to log in #' + id + '.');
    if (password==123) {
      user.id = id;
      user.name = "Test User";
      log_in_time = new Date();
      log_in_expires = moment(log_in_time).add(20, 'minute');
      deferred.resolve('Hello, #' + id + '!');
    } else {
      deferred.reject('Wrong password for user #' + id);
    }
   }, mock_delay);
  return deferred.promise;
}

function logout(){
  log_in_time = false;
  log_in_expires = false;
  user.name = false;
}

/*
* @return boolean
*/
function loggedIn(){
  return !!user.name;
}

/*
* @return boolean
*/
function loggedInOrDeny(){
  if (!loggedIn()) {
    $log.log('not authorised');
    $state.go('login');
  }
}

}})();
