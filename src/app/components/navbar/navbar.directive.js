(function() {
  'use strict';

  angular
    .module('aps')
    .directive('acmeNavbar', acmeNavbar);

  /** @ngInject */
  function acmeNavbar() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/navbar/navbar.html',
      scope: {
          creationDate: '='
      },
      controller: NavbarController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function NavbarController(mhs) {
      var vm = this;
      vm.loggedIn = mhs.loggedIn;
      vm.logout = mhs.logout;
      vm.loginTime = mhs.loginTime;
      vm.loginExpires = mhs.loginExpires;
    }
  }

})();
