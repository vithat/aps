(function() {
  'use strict';

  angular
    .module('aps')
    .filter('title', titleFilter);

  /** @ngInject */
  function titleFilter() {
    return function (text) {
      return _.startCase(text);
    };
  }
})();
