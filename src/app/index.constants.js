/* global malarkey:false, moment:false */
(function() {
  'use strict';
  angular
    .module('aps')
    .constant('malarkey', malarkey)
    .constant('moment', moment)

})();
