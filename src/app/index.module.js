(function() {
  'use strict';

  angular
    .module('aps', ['ngCookies', 'ngTouch', 'ngMessages', 'ngResource',
      'LocalStorageModule',
      'ui.router', 'ui.bootstrap', 'toastr', 'trNgGrid', 'schemaForm',
       'angularMoment', 'jsonFormatter']);


})();
