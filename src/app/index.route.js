(function() {
  'use strict';

  angular
    .module('aps')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main',
        resolve:{
          config: ['mhs', function(mhs) {
            return mhs.configPromise();
          }]}
      });

    $urlRouterProvider.otherwise('/');
  }

})();
