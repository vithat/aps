(function() {
  'use strict';

  angular
    .module('aps')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
