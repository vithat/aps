(function() {
  'use strict';

angular
  .module('aps')
  .controller('LoginController', LoginController);

/** @ngInject */
function LoginController($log, $state, mhs) {
  var vm = this;
  activate();

  function activate() {
    $log.log('starting Login.');
  }

  vm.submit = function() {
    $log.log('logging in');

    mhs.login(vm.id, vm.password).then(
    function(greeting) { // success
      $log.log('have a greeting', greeting);
      $state.go('patients');
    }, function(reason) { // failed
      alert('oh dear Failed: ' + reason);
    });
  }

}

})();
