(function() {
  'use strict';

  angular
    .module('aps')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'app/login/login.html',
        controller: 'LoginController',
        controllerAs: 'login',
        resolve:{
          config: ['mhs', function(mhs) {
            return mhs.configPromise();
          }]}
      });

  }

})();
