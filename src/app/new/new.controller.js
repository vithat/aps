(function() {
  'use strict';

angular
  .module('aps')
  .controller('NewController', NewController);

/** @ngInject */
function NewController($log, $scope, $state, toastr, apsService, mhs) {
  $log.log('starting new controller');
  var vm = this;
  var patient_id;
  activate();

  function activate() {
    mhs.loggedInOrDeny();
  }

  vm.submitx = function() {
    console.log('vm', vm);
    $state.go('register', {urn: vm.patient_id});
  }

  vm.cancel = function() {
    $log.log('cancelling');
    $state.go('patients');
  }


}
})();
