(function() {
  'use strict';

  angular
    .module('aps')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider.state('new', {
        url: '/new',
        templateUrl: 'app/new/new.html',
        controller: 'NewController',
        controllerAs: 'vm'
    });
  }

})();
