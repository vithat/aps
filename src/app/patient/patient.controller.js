(function() {
  'use strict';

angular
  .module('aps')
  .controller('PatientController', PatientController);

/** @ngInject */
function PatientController($log, $scope, $state, toastr, data, apsService, mhs) {
  $log.log('starting patient controller');
  var vm = this;
  var patient_id;
  activate();

  function activate() {
    mhs.loggedInOrDeny();
    $log.log('data', data);
    vm.rec = data;
    patient_id = vm.rec.patient.id;
    $log.log('patient_id', patient_id);
    vm.schema = apsService.reviewSchema(vm.rec);
    vm.form = apsService.reviewForm(vm.rec);
  }


  vm.model = {
    medication: [{medication: 'panadol'}, {medication: 'PCA'}]
  };

  vm.onSubmit = function() {
    // broadcast an event so all fields validate themselves
    $scope.$broadcast('schemaFormValidate');
    apsService.saveReview(patient_id, vm.model).then(
      function success(message) {
        toastr.success('Assessment saved');
        $log.log('failed save', message);
        $state.go('patients');
      },
      function fail(message) {
        toastr.error('Something bad happened');
        $log.error('failed save', message);
      })
  }

  vm.cancel = function() {
    $log.log('cancelling');
    $state.go('patients');
  }


}
})();
