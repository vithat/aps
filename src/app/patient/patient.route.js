(function() {
  'use strict';

  angular
    .module('aps')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('patient', {
        url: '/patient/:urn',
        templateUrl: 'app/patient/patient.html',
        controller: 'PatientController',
        controllerAs: 'pvm',
        resolve: {
          data: ['$log', '$stateParams', 'apsService', function($log, $stateParams,  apsService) {
            return apsService.getPatient($stateParams.urn)
              .then(
                function(patient){
                  $log.log('patient loaded', patient);
                  return patient;
                },
                function(error){
                  $log.log('failed patient load');
                  return {error:error}
                }
              );
          }]
        }
      });


  }

})();
