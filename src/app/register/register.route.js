(function() {
  'use strict';

  angular
    .module('aps')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('register', {
        url: '/patient/:urn/register',
        templateUrl: 'app/register/register.html',
        controller: 'PatientRegisterController',
        controllerAs: 'pvm',
        resolve: {
          data: ['$log', '$stateParams', 'apsService', function($log, $stateParams,  apsService) {
            return apsService.getPatient($stateParams.urn)
              .then(
                function(patient){
                  $log.log('patient loaded', patient);
                  return patient;
                },
                function(error){
                  $log.log('failed patient load');
                  return {error:error}
                }
              );
          }]
        }
      });

  }

})();
